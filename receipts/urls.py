from django.urls import path
from receipts.views import show_receipt

urlpatterns = [
    path("", show_receipt, name='show_receipts'),
    path('receipts/lists/', show_receipt, name='show_receipt'),

]
