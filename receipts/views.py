from django.shortcuts import render
from receipts.models import Receipt

# Create your views here.

def show_receipt(request):
    show_receipts = Receipt.objects.all()
    context = {
        'show_receipts': show_receipts
    }
    return render(request, 'lists/show_receipts.html', context)
